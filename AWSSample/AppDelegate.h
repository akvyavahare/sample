//
//  AppDelegate.h
//  AWSSample
//
//  Created by Akshay Vyavahare on 01/10/18.
//  Copyright © 2018 Akshay Vyavahare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

