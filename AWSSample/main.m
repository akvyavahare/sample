//
//  main.m
//  AWSSample
//
//  Created by Akshay Vyavahare on 01/10/18.
//  Copyright © 2018 Akshay Vyavahare. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
